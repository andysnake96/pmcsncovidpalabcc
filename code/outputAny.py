from lib.rngs     import plantSeeds, random, selectStream,getSeed,stream
from lib.estimate import *
from lib.acsFunc  import *

from config     import *
from current    import *
from utils      import *

from json       import dump,load
from copy         import deepcopy
from statistics   import mean,variance,stdev 
##Steady stats compute
###misured output stats tecqs

    
def transientReplicaGetN(wDevProp=.1,stop_transient_any=STOP_TRANSIENT_ANY,MAXN=500):
    out  = list()   #[(sysStat,N_for),...] trgt confIntWidth
    STOP = stop_transient_any
    RUNTIMESAMPLING = False
    accStats,vaccStats,sysStats,_seeds = list(),list(),list(),list()
    plantSeeds(SEED)
    for x in range(MAXN):
        acc,vacc,sys,_states,_outSeed = main(seed=None,stop=STOP) 
        accStats.append(acc); vaccStats.append(vacc);sysStats.append(sys)
        print("done replica %d/%d"%(x+1,MAXN),end="\r")
    for k in sys.keys():
        if sys[k] == None or isIterable(sys[k]) \
            or "_" in k or "stop" in k or "through" in k.lower():continue
        data = [ x[k] for x in sysStats ]
        n = getSizeForCustomConfIntervalW(data,loc=LOC,w_proportionDev=wDevProp)
        print("\n%s -> NumReplica for custom w=%f stdev\t\t" %(k,wDevProp),n)
        out.append( (k,n) )
        if DEBUG: 
            i = confidentIntervalAVG_Student(data,LOC)
            print("trgtW:",stdev(data)*wDevProp*2,i[1]-i[0],i)
    return out
    
def transientReplicaAny(nrepl=NREPL,stop_transient_any=STOP_TRANSIENT_ANY,loc=LOC):
    """
    unique seed init, then @nrepl run of main to gathering all the statistics
    confidence interval at @loc level of confidence level
    Returns:
    -confidence interval of each dflt statistics over the replicas' stats outs
    -dflt statistics for acceptance,vaccLines with mean,variance per each field
    """
    #TRANSIENT: RUN REPLICHE => GATHER OUTPUT STATISTICS
    global RUNTIMESAMPLING
    STOP = stop_transient_any
    RUNTIMESAMPLING = False
    print("Transient analisis with replica method: %d replicas\tstop:%d" % (nrepl,STOP),"pol:",VACC_LINE_MODE )
    accStats,vaccStats,sysStats,_seeds = list(),list(),list(),list()
    plantSeeds(SEED)
    for x in range(nrepl):
        _seeds.append(getStreamSeeds())
        acc,vacc,sys,_states,_outSeed = main(seed=None,stop=STOP) #no reinit=>8,367,782 safe call per stream 
        accStats.append(acc); vaccStats.append(vacc);sysStats.append(sys)
        print("done replica %d/%d"%(x+1,nrepl),end="\r")

    #print("Steady Stats\n acceptance :\tlambda=%f\tmu_i=%f"%(IN_LAMBDA,E_TS_I_ACCEPT**-1))
    p,pBlock,steadyMSQ_TQ,steadyMSQ_TS,steadyMSQ_NQ,steadyMSQ_N,l_rna,mu_rna,e_tq_rna,\
        e_ts_rna,l_std,mu_std,e_tq_std,e_ts_std,avgWaitSysAny = steadyVals()
    avgPopolSysSteady = avgWaitSysAny * IN_LAMBDA 
    #TODO confidence intervals da stats 
    n = nrepl
    #SYS
    sysTranConfInt= dict()
    sys.pop('avgWaitSysSingleJobAvg')
    for k in sys.keys():
        if sys[k] == None or isIterable(sys[k]):  continue
        data = [ x[k] for x in sysStats]
        if TAILOR_NREPL_PER_FIELD:   n = getSizeForCustomConfIntervalW(data,loc) 
        sysTranConfInt[k] = confidentIntervalAVG_Student(data[:n],loc)
        sysTranConfInt[k+"NREPL"] = n
    
    #ACCEPTANCE
    acceptTranConfInt= dict()
    for k in acc.keys():
        data = [ x[k] for x in accStats ]
        if isIterable(acc[k]):
            if type(acc[k][0]) == dict: #case list of dict -> servers
                serv,servers = dict(),list()
                for s in range(len(acc[k])):      #for each server
                    for ks in acc[k][s].keys():
                        servData = [ i[s][ks] for i in data ]
                        serv[ks] = confidentIntervalAVG_Student(servData,loc)
                    servers.append(serv)
                acceptTranConfInt[k] = servers
                continue
            else:   #tuple avg-stdev
                out = list()
                for s in range(len(acc[k])):
                    dataEntry = [ x[s] for x in data ]
                    out.append(confidentIntervalAVG_Student(dataEntry,loc))
                acceptTranConfInt[k] = out
                continue
        if TAILOR_NREPL_PER_FIELD:   n = getSizeForCustomConfIntervalW(data,loc) 
        acceptTranConfInt[k] = confidentIntervalAVG_Student(data[:n],loc)
        acceptTranConfInt[k+"NREPL"] = n
    
    #VACC LINES
    vaccLinesTranConfInt = deepcopy(vacc)#vacc line entries'll be overwritten
    for v in range(len(vaccLinesTranConfInt)): #stat 4 v-th vacc line
        for k in vacc[0].keys():
            #overwrite (last) v-th vacc line with the aggregated stat
            data = [ x[v][k] for x in vaccStats ]
            if isIterable(vacc[0][k]): #only avg-dev tuple here
                continue
                out = list()
                for i in range(len(vacc[0][k])):
                    data = [x[i] for x in data]
                    n = getSizeForCustomConfIntervalW(data,loc) 
                    out.append(confidentIntervalAVG_Student(data[:n],loc))
                vaccLinesTranConfInt[v][k] = out
                continue
            if TAILOR_NREPL_PER_FIELD:n = getSizeForCustomConfIntervalW(data,loc) 
            vaccLinesTranConfInt[v][k] = confidentIntervalAVG_Student(data[:n],loc)
            vaccLinesTranConfInt[v][k+"NREPL"] = n

    #TODO AVG UTILIZZATION
    vaccGlobTranConfInt = {}
    vaccStatsRna      =[v[:SSQ_RNA_N] for v in vaccStats]
    vaccStatsStd      =[v[SSQ_RNA_N:] for v in vaccStats]
    getAvgUtil        = lambda l:  mean( [x["utilization"] for x in l] )
    vaccRnaAvgUtiliz  = [ getAvgUtil(l) for l in vaccStatsRna ]
    vaccStdAvgUtiliz  = [ getAvgUtil(l) for l in vaccStatsStd ]
    vaccRnaAvgUtilizN = getSizeForCustomConfIntervalW(vaccRnaAvgUtiliz)
    vaccStdAvgUtilizN = getSizeForCustomConfIntervalW(vaccStdAvgUtiliz)
    vaccGlobTranConfInt["avgUtilizRNA"]=confidentIntervalAVG_Student(\
        vaccRnaAvgUtiliz[:vaccRnaAvgUtilizN],loc)
    vaccGlobTranConfInt["avgUtilizRNANREPL"]=vaccRnaAvgUtilizN
    vaccGlobTranConfInt["avgUtilizSTD"]=confidentIntervalAVG_Student(\
        vaccStdAvgUtiliz[:vaccStdAvgUtilizN],loc)
    vaccGlobTranConfInt["avgUtilizSTDNREPL"]=vaccStdAvgUtilizN

    #TODO USELESS??? mean/dev for each sampled data from replica
    #ACCEPTANCE
    acceptTranStats    = dict()
    vaccLinesTranStats = deepcopy(vacc)   #vacc line entries'll be overwritten
    for k in acc.keys():
        data = [ x[k] for x in accStats ]
        if isIterable(acc[k]):    #special case list of dict -> servers
            if type(acc[k][0]) == dict: #case list of dict -> servers
                serv,servers = dict(),list()
                for s in range(len(acc[k])):      #for each server
                    for ks in acc[k][s].keys():
                        d = [ i[s][ks] for i in data ]
                        serv[ks] = (mean(d),variance(d)) 
                    servers.append(serv)
                acceptTranStats[k] = servers
                continue
            else:   #tuple avg-stdev
                out = list()
                for s in range(len(acc[k])):
                    dataEntry = [ x[s] for x in data ]
                    out.append((mean(dataEntry),variance(dataEntry)))
                acceptTranConfInt[k] = out
                continue
        acceptTranStats[k] = (mean(data),variance(data))
    
    #VACC LINES
    for v in range(len(vaccLinesTranStats)): #stat for the v-th vacc line
        for k in vacc[0].keys():
            #overwrite (last) v-th vacc line with the aggregated stat
            data = [ x[v][k] for x in vaccStats ]
            if isIterable(vacc[0][k]): #only avg-dev tuple here
                out = list()
                for i in range(len(vacc[0][k])):
                    dataEntry = [x[i] for x in data]
                    out.append((mean(dataEntry),variance(dataEntry)))
                continue
            vaccLinesTranStats[v][k] = (mean(data),variance(data))
    
    _FMTL = 6
    print("replica method any with",nrepl,"replicas and STOP=",stop_transient_any,"LOC=",loc)
    print("\nSteady in confIntervals:\nSYS")
    print("X:".ljust(_FMTL),inPrint(IN_LAMBDA,sysTranConfInt["throughputSys"]),sysTranConfInt["throughputSysNREPL"])
    print("E[T_S]:".ljust(_FMTL),inPrint(avgWaitSysAny,sysTranConfInt["avgWaitSysLittle"]),sysTranConfInt["avgWaitSysLittleNREPL"])
    print("E[N]:".ljust(_FMTL),inPrint(avgPopolSysSteady,sysTranConfInt["avgPopolSysArea"]),sysTranConfInt["avgPopolSysAreaNREPL"])
    print("\nSteady in confIntervals SINGLE NODES:\nMSQ Acceptance")
    print("l:".ljust(_FMTL),     inPrint(1/IN_LAMBDA,acceptTranConfInt["avgInterrarivals"]),acceptTranConfInt["avgInterrarivalsNREPL"])
    print("E[T_Q]:".ljust(_FMTL),inPrint(steadyMSQ_TQ,acceptTranConfInt["avgDelayLittle"]),acceptTranConfInt["avgDelayLittleNREPL"])
    print("E[T_S]:".ljust(_FMTL),inPrint(steadyMSQ_TS,acceptTranConfInt["avgWaitLittle"]),acceptTranConfInt["avgWaitLittleNREPL"])
    print("E[N_Q]:".ljust(_FMTL),inPrint(steadyMSQ_NQ,acceptTranConfInt["avg#InQueue"]),acceptTranConfInt["avg#InQueueNREPL"])
    print("E[N]:".ljust(_FMTL),  inPrint(steadyMSQ_N,acceptTranConfInt["avg#InNode"]),acceptTranConfInt["avg#InNodeNREPL"])
    #print("Throughput:".ljust(_FMTL),inPrint(IN_LAMBDA,acceptTranConfInt["avgThroughput"]))
    for kind in [RNA,STD]:
        if kind==RNA:
            print("\nRNA lines")
            l,mu,e_tq,e_ts = l_rna,mu_rna,e_tq_rna,e_ts_rna
            lines = vaccLinesTranConfInt[:SSQ_RNA_N]
            print("p:".ljust(_FMTL),inPrint(l/mu,vaccGlobTranConfInt["avgUtilizRNA"]),vaccGlobTranConfInt["avgUtilizRNANREPL"])
        else:
            print("\nSTD lines")
            l,mu,e_tq,e_ts = l_std,mu_std,e_tq_std,e_ts_std
            lines = vaccLinesTranConfInt[SSQ_RNA_N:]
            print("p:".ljust(_FMTL),inPrint(l/mu,vaccGlobTranConfInt["avgUtilizSTD"]),vaccGlobTranConfInt["avgUtilizSTDNREPL"])
        for i,line in enumerate(lines):
            print("line",i,":")
            print((" l").ljust(_FMTL),     inPrint(1/l,line["avgInterrarivals"]),line["avgInterrarivalsNREPL"])
            #print(("\tmu").ljust(_FMTL),    inPrint(1/mu,line["avgService"]))
            print((" E[T_Q]").ljust(_FMTL),inPrint(e_tq,line["avgDelayLittle"]),line["avgDelayLittleNREPL"])
            print((" E[T_S]").ljust(_FMTL),inPrint(e_ts,line["avgWaitLittle"]),line["avgWaitLittleNREPL"])
            print((" p").ljust(_FMTL),inPrint(l/mu,line["utilization"]),line["utilizationNREPL"])
            #print(("\tThroughput").ljust(_FMTL),inPrint(l,line["avgThroughput"]))
    if AUDIT :
        smartP(acceptTranConfInt,"acceptTranConfInt")
        smartP(acceptTranStats,"acceptTranStats")
        smartP(vaccLinesTranConfInt,"vaccLinesTranConfInt")
        smartP(vaccLinesTranStats,"vaccLinesTranStats")
    return acceptTranConfInt,acceptTranStats,\
            vaccLinesTranConfInt,vaccLinesTranStats

def steadyBatchMeansAny(dur,batchSize,statExtr,poll=1,loc=LOC,\
    samplingInSysOutState=None,sampleAutoCorrelationPlot=False,avgDev1pass=True,\
    title="sysStat"):
    """
    batch means any with simulation calibration 
    @dur:               simulation total duration
    @batchSize:         size of each batch for the analisys
    @statExtr:          function to apply over stat object to extract only target stats
    @poll:              sampling interval (in accord to SAMPLING_MODE)
    @getStat:           function that extract stats of intrest in
                        sampled states, where apply batch means
    @samplingInSysOutState: function that'll extract samples from out sysStat
    @avgDev1pass: use welford1pass algo to compute batches avg,stdev
    
    Returns: list of confidence interval for each extracted stat of intrest 
             by use of batch means method
    """
    #global SAMPLING_MODE,STOP_MODE
    p,pBlock,steadyMSQ_TQ,steadyMSQ_TS,steadyMSQ_NQ,steadyMSQ_N,l_rna,mu_rna,e_tq_rna,\
        e_ts_rna,l_std,mu_std,e_tq_std,e_ts_std,avgWaitSysAny = steadyVals()
    p_rna , p_std = l_rna/mu_rna , l_std/mu_std
    throughputSysAny  = IN_LAMBDA 
    avgPopolSysSteady = avgWaitSysAny * IN_LAMBDA
    print("SteadyVals:\tE[T_s]_sys:",avgWaitSysAny,"E[N]_sys:",avgPopolSysSteady,\
          "E[X]_sys",throughputSysAny,end="\n\n")
    #SAMPLING_MODE="TIME"
    sstateConfInt = list()  
    _accStats,_vaccStats,sysStats,sampledStates,_outSeed = \
         main(stop=dur,sample_stats=poll,statExtr=statExtr)
    trgt = sampledStates
    if samplingInSysOutState!=None: 
        trgt=samplingInSysOutState(sysStats)
        stats = trgt
    else:   times,stats = [x[0] for x in trgt],[x[1] for x in trgt]
    k,r = int(len(trgt)/batchSize),len(trgt)%batchSize
    print("BatchMeans with:\tbatchSize",batchSize,"k",k,"simulLen",dur,\
        "-> #samples",len(trgt),"sampling each",poll,SAMPLING_MODE,VACC_LINE_MODE)
    assert k > 1, "batchsize too big or too few samples"
    batches = [ stats[ x*batchSize:(x+1)*batchSize ] for x in range(k) ]
    #averaging function config
    avg_sdev = lambda data: (mean(data),stdev(data))
    if avgDev1pass:   avg_sdev = welford1passAVG_STDEV
    #apply processing to sampled data field[s separatelly in case]
    fields,fieldExtract = [title],fullCopy    #1field data
    s = stats[0]
    if isIterable(s): fields,fieldExtract = s._fields,lambda obj,f: getattr(obj,f)
    for f in fields:
        print("\nAnalizing :",f,"autocorrelations:")
        #batchStats=[avg_sdev([fieldExtract(x,f) for x in b]) for b in batches]
        batchAvgs=[ mean([fieldExtract(x,f) for x in b]) for b in batches ]
        avg,stddev,autocorr,autocov=sampleAutoCorrelation(batchAvgs,K=min(80,K_BATCH_N-1))
        printFloatsShorted(autocorr)
        if autocorr[0] >.2: continue
        print(HIGHLIGHTED % (f+":\t< .2 AUTOCORRELATION GUIDELINE PASSED"))
        if sampleAutoCorrelationPlot:
            steamPlot(autocorr,title=f+"  K=%d  B=%d"%(k,batchSize))
        confInt=confidentIntervalAVG_Student(batchAvgs,loc,avgDev1pass)
        sstateConfInt.append((f,confInt))
        #TODO PRETTY PRINT WITH STEADY VAL
        if samplingInSysOutState:   print(inPrint(avgWaitSysAny,confInt))
        elif statExtr == getPopol : print(inPrint(avgPopolSysSteady,confInt))
        elif "rna" in f.lower():    print(inPrint(p_rna,confInt))
        elif "std" in f.lower():    print(inPrint(p_std,confInt))
    return sstateConfInt
   
def _sysRunIncreasing(stopTimeRange,seeds,runtimeSampling=False,dumpFile=True):
    sysWaitsAvg,sysPopAvg,sysThroughputs,stops = list(),list(),list() ,list() 
    for stop in stopTimeRange:
        print("running with stopLen:",stop,"/",max(stopTimeRange),"seed:%9d"%seeds[0],end="\r")
        accStats,vaccStats,sysStats,_states,endingSeeds = main(seed=seeds,stop=stop)
        sysWaitsAvg.append(sysStats["avgWaitSysLittle"])
        sysPopAvg.append(sysStats["avgPopolSysArea"])
        sysThroughputs.append(sysStats["throughputSys"])
        stops.append((stop,sysStats["stopTime"]))
    print("")
    if dumpFile:
        _trgt={"sysWaitsAvg":sysWaitsAvg,"sysPopAvg":sysPopAvg,\
            "sysThroughputs":sysThroughputs,"stops":stops}
        with open("/tmp/steadyS_seed-%d.json"%seeds[0],"w") as f:   dump(_trgt,f)
    return sysWaitsAvg,sysPopAvg,sysThroughputs,stops,endingSeeds

def sysRunIncreasing(searchRange,startSeed=DFLT_SEED,dumpedData=None):    #MODIFY STOP_MODE
    """
    STEADINESS SEARCH: long runs over @searchRange long simulations with 3 seeds
    if @dumpedData!=None: extract data from 
    [(seed,{stops:[...],sysWaitsAvg:[...],sysPopAvg:[...],sysThroughputs:[...]}),(seed2,..)..]
    """
    print("increasingly long system runs: stopMode",STOP_MODE,"pol:",VACC_LINE_MODE)
    #run with different seed incresengly
    plantSeeds(startSeed)
    startSeeds = getStreamSeeds()
    if dumpedData == None:
        sysWaitsAvg1,sysPopAvg1,sysThroughputs1,stops1,seedEnds1=_sysRunIncreasing(searchRange,startSeeds)
        sysWaitsAvg2,sysPopAvg2,sysThroughputs2,stops2,seedEnds2=_sysRunIncreasing(searchRange,seedEnds1)
        sysWaitsAvg3,sysPopAvg3,sysThroughputs3,stops3,seedEnds3=_sysRunIncreasing(searchRange,seedEnds2)
    else:
        seed1,seed2,seed3 = [t[0] for t in dumpedData]
        _unpack=lambda d: (d["sysWaitsAvg"],d["sysPopAvg"],d["sysThroughputs"],d["stops"])
        sysWaitsAvg1,sysPopAvg1,sysThroughputs1,stops1=_unpack(dumpedData[0][1])
        sysWaitsAvg2,sysPopAvg2,sysThroughputs2,stops2=_unpack(dumpedData[1][1])
        sysWaitsAvg3,sysPopAvg3,sysThroughputs3,stops3=_unpack(dumpedData[2][1])
        searchRange = [x[0] for x in stops1]
     
    seed1,seed2,seed3 = startSeed,seedEnds1[0],seedEnds2[0]
    p,pBlock,steadyMSQ_TQ,steadyMSQ_TS,steadyMSQ_NQ,steadyMSQ_N,l_rna,mu_rna,e_tq_rna,\
        e_ts_rna,l_std,mu_std,e_tq_std,e_ts_std,avgWaitSysAny = steadyVals()
    avgPopolSysSteady = avgWaitSysAny * IN_LAMBDA
    #plots
    print("Steady avgWaitSysLittleAnalitical:",avgWaitSysAny,end="\n\n")
    sysWaitsAvgAny = [avgWaitSysAny] * len(searchRange)
    plotMultiOverlayWrap(searchRange,"E[T_S]_sys",\
        [sysWaitsAvgAny,sysWaitsAvg1,sysWaitsAvg2,sysWaitsAvg3],\
        ["sysWaitsAvg-ANY"]+["sysWaitsAvg%d"%seed for seed in [seed1,seed2,seed3]])
    print("Steady avgPopolSysAnalitical:",avgPopolSysSteady,end="\n\n")
    sysPopAny = [avgPopolSysSteady]  * len(searchRange)
    plotMultiOverlayWrap(searchRange,"E[N]_sys",\
        [sysPopAny,sysPopAvg1,sysPopAvg2,sysPopAvg3],\
        ["sysPopolation-ANY"]+["sysPoplationAvg%d"%seed for seed in [seed1,seed2,seed3]])
    print("Steady throughputSysAny:",IN_LAMBDA,end="\n\n")
    sysThrouputAny = [IN_LAMBDA]  * len(searchRange)
    plotMultiOverlayWrap(searchRange,"E[X]",\
        [sysThrouputAny,sysThroughputs1,sysThroughputs2,sysThroughputs3],\
        ["sysThrouput-ANY"]+["sysThrouput%d"%seed for seed in [seed1,seed2,seed3]])
if __name__ == "__main__":
    #global AUDIT,DEBUG,STOP_MODE,SAMPLING_MODE
    #AUDIT_OLD,DEBUG_OLD = AUDIT,DEBUG;#AUDIT,DEBUG = False,False

    #TRANSIENT ANY
    #nForCustom_w = transientReplicaGetN()
    #transientReplicaAny(nrepl=max(x[1] for x in nForCustom_w));exit(0)
    #transientReplicaAny();exit(0)
    
    #STEADY    ANY
    #sysRunIncreasing(range(600,int(STOP_STEADY_SEARCH),2200));   exit(0)

    #BATCH MEANS
    #out=steadyBatchMeansAny(K_BATCH_N*B_BATCH_S*SAMPLE_STATS,B_BATCH_S,getVaccLines_p,\
    #   SAMPLE_STATS,sampleAutoCorrelationPlot=False);exit(0)
    #fish increase search: for i in (seq 1 33); export B_BATCH_S=(math "10+10*$i") K_BATCH_N=64 SAMPLE_STATS=1; python3 current.py;end
    #Poplation batchSize 220 k 64 simulLen 14080 -> #samples 14081 sampling each 1 JOBS_OUT
    out=steadyBatchMeansAny(K_BATCH_N*B_BATCH_S*SAMPLE_STATS,B_BATCH_S,getPopol,\
       SAMPLE_STATS,sampleAutoCorrelationPlot=False)
    #sysJobWaits:   -> k,b,poll=64,90,3    ||   160,25,3 -> (531.4265886311654,651.0977196207185)
    #out=steadyBatchMeansAny(K_BATCH_N*B_BATCH_S*SAMPLE_STATS,B_BATCH_S,None,SAMPLE_STATS,\
    #  samplingInSysOutState=lambda sysState: sysState["sysJobWaits"],title="sysJobWaits")
    print(out)
    exit(0)
    #dumped plot
    fpaths="steadyS_seed-123456789.json  steadyS_seed-222143169.json  steadyS_seed-987654321.json".split()
    _getOnlyNumbers = lambda s: int("".join(filter(lambda c: c.isdigit(),s)))
    dumped = list()
    for p in fpaths:
        with open(p) as f:  dumped.append( (_getOnlyNumbers(p),load(f)) )
    sysRunIncreasing(None,dumped)
