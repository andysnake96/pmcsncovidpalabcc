#Entities for the current system
from collections    import namedtuple 
from statistics     import mean,stdev,variance

from config         import * 
from lib.estimate   import welford1passRollingAVG 
from utils          import  *

class Time:
    def __init__(self):
        self.curr = START #curr time 
        self.next = None #next event time (next kw is reserved in py)
        
        self.generatedJobsN  = 0
        self.compleatedJobsN = 0

########    Events
class Event:
    def __init__(self,time=0):
        self.t        = time    #event time, -1 for disabled
        self.creation = time    #event creation time

class _EventSSQ:
    def __init__(self,kind):
        self.arrival     = Event()
        self.completion  = Event()

        self.kind        = kind

class _EventMSQ:
    def __init__(self,msq_servers_n=MSQ_SERVERS_N):
        self.arrival     = Event()
        #for each server:  [lastCompletitiontime,jobArrivalNodeRef]
        self.completions = [ Event() for x in range(msq_servers_n) ]

    def findAvailServer(s): 
        #@eventsMSQ: _EventMSQ obj wrapping msq events
        #return idx of an avail server[idle longest]
        i = 0
        while isEnabled(s.completions[i]):  i += 1
        trgt = i    #idx of the first idle server 
        if not FIND_IDLE_MOST_SERVER:       return trgt
        while i+1 < len(s.completions):  #find/ensure server s is the idle from longest time
          i += 1
          if not isEnabled(s.completions[i]) \
             and -s.completions[i].t < -s.completions[trgt].t:
            trgt=i
        return trgt
    def getNextCompletition(s):
        minTime,out,idx=INFTY,s.completions[0],0
        for i,c in enumerate(s.completions):
            if isEnabled(c) and c.t < minTime:    minTime,out,idx = c.t,c,i
        return out,idx

class Events:
    def __init__(self,ssq_rna_n=SSQ_RNA_N,ssq_std_n=SSQ_STD_N,msq_servers_n=MSQ_SERVERS_N):
        self.MSQAcceptance = _EventMSQ(msq_servers_n)

        self.rnaSSQ = [ _EventSSQ(RNA) for x in range(ssq_rna_n) ]
        self.stdSSQ = [ _EventSSQ(STD) for x in range(ssq_std_n) ]
        #wrap a list with references too all ssq lines objs
        self.allSSQ = self.rnaSSQ.copy()
        self.allSSQ.extend(self.stdSSQ)
        self.genSSQ = _EventSSQ("GEN")
        self.allSSQGen = self.allSSQ.copy() + [self.genSSQ]

    def getNextEvent(self): 
        #Returns (nextEvent ,nextEventTypeStr,idxInEventsGroup)
        minComplMSQ,idxComplMSQ      =self.MSQAcceptance.getNextCompletition()
        _t,idxComplSSQrna,minComplSSQrna=min_argMinEvent([x.completion for x in self.rnaSSQ ])
        _t,idxComplSSQstd,minComplSSQstd=min_argMinEvent([x.completion for x in self.stdSSQ ])
        _t,idxminArrSSQrna,minArrSSQrna =min_argMinEvent([x.arrival    for x in self.rnaSSQ ])
        _t,idxminArrSSQstd,minArrSSQstd =min_argMinEvent([x.arrival    for x in self.stdSSQ ])

        genArr,genCompl =self.genSSQ.arrival,self.genSSQ.completion  
        events = [ minComplMSQ, minComplSSQrna, minComplSSQstd,
                   self.MSQAcceptance.arrival, minArrSSQrna, minArrSSQstd,  genArr,genCompl]
 
        neTime,_,ne = min((e.t,i,e) for i,e in enumerate(events) if isEnabled(e))
        #TODO runtime exception if called with all events disarmed 
    
        msqArrival = self.MSQAcceptance.arrival #_wrap
        #get also the type 
        if   ne == msqArrival:     neType,neIdx = "arrAccept",None
        elif ne == minComplMSQ:    neType,neIdx = "compAccept",idxComplMSQ
        elif ne == minArrSSQrna:   neType,neIdx = "arrSSQrna",idxminArrSSQrna
        elif ne == minArrSSQstd:   neType,neIdx = "arrSSQstd",idxminArrSSQstd
        elif ne == minComplSSQrna: neType,neIdx = "compSSQrna",idxComplSSQrna
        elif ne == minComplSSQstd: neType,neIdx = "compSSQstd",idxComplSSQstd

        elif ne == genArr:         neType,neIdx = "arrSSQGen",0
        elif ne == genCompl:       neType,neIdx = "compSSQGen",0
        else:                          raise Exception("INVALID N.E.")

        return (ne,neType,neIdx)

    def isAnyEnabled(self):
        enabled=[isEnabled(self.MSQAcceptance.arrival),\
            isEnabled(self.MSQAcceptance.getNextCompletition()[0])]
        enabled += [isEnabled(x.arrival) for x in self.allSSQ]
        enabled += [isEnabled(x.completion) for x in self.allSSQ]
        
        enabled += [isEnabled(self.genSSQ.arrival),isEnabled(self.genSSQ.completion)]
        return any(enabled)

########    State -> Stats/Cumulatives
class _ServerCumul:               #MSQ center
    def __init__(self):
        self.service = 0.0        #service times 
        self.served  = 0          #number served 
class _MSQ:
    def __init__(self,msq_servers_n=MSQ_SERVERS_N):
        self.index    = 0
        self.jInQueue = list() #(arrived jobs' arrival time,event)
        self.jInService = 0    #counter of jobs in service

        self.servers  = [ _ServerCumul() for i in range(msq_servers_n) ]
        self.area     = 0.0
        self.waits    = list()  #wait time direct compute
        self.waitAvgWelford = 0.0
        self._sum_waitAvgWelford = 0.0   #aux for rolling E[waits]
    def __len__(self):  return len(self.jInQueue) + self.jInService
    def addWait(self,delay_wait):
        self.waits.append(delay_wait)
        self.waitAvgWelford,self._sum_waitAvgWelford = welford1passRollingAVG(\
            delay_wait[1],len(self.waits),self._sum_waitAvgWelford,self.waitAvgWelford)
    def getQueueArea(self): #get queue \int{n(t)} area removing servers
        area = self.area
        for s in self.servers:  area -= s.service
        return area

    def getDelay():
        if self.index == 0:    return 0
        #delay=self.getQueueArea() / self.index
        delay= mean([x[0] for x in self.waits])
        return delay

    def getWait(self):
        if self.index == 0:    return 0
        #wait= self.area / self.index
        #wait= mean([x[1] for x in self.waits])
        return self.waitAvgWelford

class _SSQ:
    def __init__(self):
        self.index          = 0 
        self.jIn            = list()
        
        self.areaFull       = 0.0
        self.areaService    = 0.0        #RB beyond FP seq op: area queue=full-service
        self.waits          = list()     #wait time direct compute
        self.waitAvgWelford = 0.0        #rolling E[waits]
        self._sum_waitAvgWelford = 0.0   #aux for rolling E[waits]
    def addWait(self,delay_wait):
        self.waits.append(delay_wait)
        self.waitAvgWelford,self._sum_waitAvgWelford = welford1passRollingAVG(\
            delay_wait[1],len(self.waits),self._sum_waitAvgWelford,self.waitAvgWelford)

    def getDelay(self):
        if self.index == 0:    return 0#None
        #delay= (self.areaFull - self.areaService) / self.index
        delay= mean([x[0] for x in self.waits])
        return delay
        
    def getWait(self):   
        if self.index == 0:    return 0 #None
        #wait= self.areaFull / self.index
        #wait= mean([x[1] for x in self.waits])
        return self.waitAvgWelford

    
class State:    #sys state & cumulative stats
    def __init__(self,ssq_rna_n=SSQ_RNA_N,ssq_std_n=SSQ_STD_N,msq_servers_n=MSQ_SERVERS_N):
        self.MSQAcceptance= _MSQ(msq_servers_n)

        self.rnaSSQ  = [ _SSQ() for x in range(ssq_rna_n) ]
        self.stdSSQ  = [ _SSQ() for x in range(ssq_std_n) ]
        #wrap a list with references too all ssq lines objs
        self.allSSQ  = self.rnaSSQ.copy()
        self.allSSQ.extend(self.stdSSQ)
        self.genSSQ  = _SSQ()
        self.allSSQG = self.allSSQ.copy() + [self.genSSQ]
        self.sysJobWaits = list()   #jobs wait time 
        nodeStatFields = [N_SYS,N_ACCEPTANCE]+ \
                         [N_RNA % i for i in range(ssq_rna_n) ] + \
                         [N_STD % i for i in range(ssq_std_n) ]
        self._Waits           = namedtuple("Waits", nodeStatFields)
        self._Delays          = namedtuple("Delays",nodeStatFields)
        self._PopolationTot   = namedtuple("PopolationTot",nodeStatFields)
        self._PopolationQueue = namedtuple("PopolationQueue",nodeStatFields)

    def existsWork(self):   #True if still exist something to process
        return len(self.MSQAcceptance) \
            + sum(len(s.jIn) for s in self.allSSQG) > 0
    #OUT STATS EXTRACTION
    def getDelays(self):     
        """Returns: current cumulative delay time for sys and each node
                    (msqAcceptance,ssqRna0,...,ssqStd0,...)
        """
        out =  [ self.MSQAcceptance.getDelay() ]
        out += [ vsL.getDelay() for vsL in self.allSSQ ]
        return self._Delays(None,*out)

    def getWaits(self,justSys=True):     
        """Returns: current cumulative wait time for sys and each node
                    (msqAcceptance,ssqRna0,...,ssqStd0,...)
        """
        msqAccept = self.MSQAcceptance.getWait()
        vaccLines = [ vsL.getWait() for vsL in self.allSSQG ]
        allOut    = sum(x.index for x in self.allSSQG)
        avgVaccLinesWaits = 0
        if allOut:  avgVaccLinesWaits=sum( vaccLines[i]*v.index/allOut \
                        for i,v in enumerate(self.allSSQG) )
        
        sys       = msqAccept + avgVaccLinesWaits
        if justSys: return sys
        return self._Waits(sys,msqAccept,*vaccLines)

    def getPopolations(self,justSys=True):
        acc   =  len(self.MSQAcceptance)
        lines =  [len(ssq.jIn) for ssq in self.allSSQG]
        sys =    acc + sum(lines)
        if justSys: return sys
        return self._PopolationTot(sys,acc,*lines)

    def getAvgPopolation(self,tCurr,justQueue=False,justSys=True):
        """
        Returns: current (at time @tCurr) avg popolation 
        (only in queue if @justQueue for each node  
        (sys,msqAcceptance,ssqRna0,...,ssqStd0,...)
        """
        trgtOutNamedTup = self._PopolationTot 
        msqArea = self.MSQAcceptance.area
        ssqAreas = [ ssq.areaFull for ssq in self.allSSQ ]
        if justQueue:
            trgtOutNamedTup = self._PopolationQueue
            msqArea = self.MSQAcceptance.getQueueArea()
            ssqAreas=[ssq.areaFull-ssq.areaService for ssq in self.allSSQ]
        nds =  [ msqArea / tCurr ]
        nds += [ area / tCurr for area in ssqAreas ]
        #assert abs(sys - (msqArea + sum(ssqAreas))/tCurr ) < THREASH_DELTA
        if justSys: return sys
        return trgtOutNamedTup(sys,*nds)

##AUX

#TODO CHECK
cc=0
class _DLL_Node:
    def __init__(self,val):
        global cc
        cc += 1
        self.val  = val
        self.next = None
        self.prev = None
        self.flag = False #in work
    def remove(self):
        global cc
        cc -= 1
        if self.next != None:   self.next.prev = self.prev
        if self.prev != None:   self.prev.next = self.next
        self.val *= -1   #TODO
        return self.val
        
class DLL:
    def __init__(self):
        self.head,self.tail = None,None
        self.len = 0
        self.lastTODO    = list()
        self.lastVersion = set()
        
    def append(self,val):
        node = _DLL_Node(val)
        self.len += 1
        if  self.head == None:
            self.head = node 
            self.tail = node 
            if DEBUG:   print("DLL RESTART")    #TODO
            assert self._checks("append",node.val ) #TODO
            return  node
        #append
        self.tail.next  = node 
        node.prev       = self.tail
        self.tail       = node
        assert self._checks("append",node.val) #TODO
        return node

    def pop(self,node):
        assert self.len > 0 and node.val > 0
        self.len -= 1
        #update shortcut refs
        out = node.remove()     #update linking
        if self.head == node:   self.head = node.next
        if self.tail == node:   self.tail = node.prev
        assert self._checks("pop",out) #TODO
        return out

    def getFirstUnflagged(self):
        n = self.head
        while  n != None and n.flag:    n = n.next      #skip busy nodes
        assert n != None,"all flagged"
        return n 

    def _traverse(self):
        l = list()
        n = self.head
        while  n != None:    l.append(n.val); n=n.next
        return l

    def __len__(self):          return self.len
    def _checks(self,op,last): #TODO
        if self.head != None:   self.head.prev == None
        if self.tail != None:   self.tail.next == None
        if self.len==0:  assert self.head == self.tail and self.head == None
    
        assert op != "pop" or -last in self.lastVersion, "wtf popped"
        dll = self._traverse()
        for x in dll: assert x > 0
        #if len(self.lastTODO) == 5000:   self.lastTODO.clear()
        self.lastTODO.append((dll,op,last))
        
        self.lastVersion = set(dll)
        assert self.len == len(dll)
        return True
    def _printHistory(self):
        for x,y,z in self.lastTODO: print(x,y,z)

if __name__ == "__main__":
    #DLL TEST
    from random import shuffle
    dll = DLL()
    nodes = list()
    for x in range(100): nodes.append(dll.append(x))
    v = dll.tail.prev.prev.prev
    print(v.__dict__)
    shuffle(nodes)
    for n in nodes: print("removed:",dll.pop(n));dll._traverse()
    nodes.clear()
    for x in range(100): nodes.append(dll.append(x))
    shuffle(nodes)
    for n in nodes: print("removed:",dll.pop(n));dll._traverse()
    assert(len(dll._traverse())) == 0
