from config import *
from utils import max_argMax

rnaN = SSQ_RNA_N        #+1
stdN = SSQ_STD_N        #+1
allN = rnaN + stdN
inLambda = IN_LAMBDA

S_acc = E_TS_I_ACCEPT *.75


demands      = [1*S_acc,P_RNA/rnaN * E_TS_I_RNA,P_STD/stdN * E_TS_I_STD]
demandsNodes = ["Acc","RNA_SSQ","STD_SSQ"]
dMax,dMaxIdx = max_argMax(demands)

print("Using %f of VaccinationResources for RNA = %f of TOT users" % (rnaN/allN,P_RNA))
print("Using %f of VaccinationResources for STD = %f of TOT users" % (stdN/allN,P_STD))
print("maxDemand:",dMax,"at",demandsNodes[dMaxIdx],"max X bound:",1/dMax,"vs",inLambda)
for i in range(len(demands)):   print(demandsNodes[i],demands[i],end=" ")
for x in range(5):
    inLambda *= 1+.20*x
    utiliz  = [ inLambda*E_TS_I_ACCEPT/MSQ_SERVERS_N,\
        inLambda*(P_RNA/rnaN)* E_TS_I_RNA,inLambda*(P_STD/stdN)* E_TS_I_STD ]
    
    uMax,uMaxIdx = max_argMax(utiliz)
    print("\nUtilization max with inLambda:",inLambda)
    print(uMax,uMaxIdx)
