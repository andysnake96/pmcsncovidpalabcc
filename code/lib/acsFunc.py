# # -------------------------------------------------------------------------  
#  * This program is based on a one-pass algorithm for the calculation of an  
#  * array of autocorrelations r[1], r[2], ... r[K].  The key feature of this 
#  * algorithm is the circular array 'hold' which stores the (K + 1) most 
#  * recent data points and the associated index 'p' which points to the 
#  * (rotating) head of the array. 
#  * 
#  * Data is read from a text file in the format 1-data-point-per-line (with 
#  * no blank lines).  Similar to programs UVS and BVS, this program is
#  * designed to be used with OS redirection. 
#  * 
#  * NOTE: the constant K (maximum lag) MUST be smaller than the # of data 
#  * points in the text file, n.  Moreover, if the autocorrelations are to be 
#  * statistically meaningful, K should be MUCH smaller than n. 
#  *
#  * Name              : acs.c  (AutoCorrelation Statistics) 
#  * Author            : Steve Park & Dave Geyer 
#  * Language          : ANSI C
#  * Latest Revision   : 2-10-97 
#  * Compile with      : gcc -lm acs.c
#  * Execute with      : acs.out < acs.dat
#  # Translated by     : Philip Steele 
#  # Language          : Python 3.3
#  # Latest Revision   : 3/26/14
#  * Execute with      : python acs.py < acs.dat
#  * ------------------------------------------------------------------------- 
#  */

#include <stdio.h>
#include <math.h>

import sys
from math import sqrt

def sampleAutoCorrelation(data,K=50):
    """
    compute sampleAutoCorrelation for lag j in (0,K] of points in @data list
    Returns: 
      @mean,dev
      @lists of the last K entries related to progressive lag j in (0,K) of 
        -autocorrelations r_j
        -autocovariance   c_j
    """
    assert K < len(data)
    SIZE = (K + 1)
    
    i = 0                          # data point index              */
    _sum = 0.0                      # sums x[i]                     */
    j = 0                          # lag index                     */
    hold = []                      # K + 1 most recent data points */
    p = 0                          # points to the head of 'hold'  */
    cosum = [0 for i in range(0,SIZE)]    # cosum[j] _sums x[i] * x[i+j]   */
    
    for a in range(SIZE):
      x = data[a]
      _sum += x
      hold.append(x)
      i += 1
    
    for x in data[SIZE:]:   #init hold with first K+1 vals
      for j in range(SIZE):
        cosum[j] += hold[p] * hold[(p + j) % SIZE]
      _sum    += x
      hold[p] = x
      p       = (p + 1) % SIZE
      i += 1 
    
    n = i #the total number of data points
    
    while (i < n + SIZE):         # empty the circular array */
      for j in range(0,SIZE):
        cosum[j] += hold[p] * hold[(p + j) % SIZE]
      hold[p] = 0.0
      p       = (p + 1) % SIZE
      i += 1 
    #EndWhile
    
    mean = _sum / n
    for j in range(0,K+1):  cosum[j] = (cosum[j] / (n - j)) - (mean * mean)
    autocorrelations = [ c_j/cosum[0] for c_j in cosum[1:] ]
    
    return mean,sqrt(cosum[0]),autocorrelations,cosum 

if __name__ == "__main__":
    with open("acs.dat") as f:  data=[float(x) for x in f.readlines()]
    mean,stdev,autocorrelations,autocovariances = sampleAutoCorrelation(data) 
    print(mean,stdev)
    for j,r in enumerate(autocorrelations):  print(j+1,r)
    
