from math import sqrt
from statistics import mean,stdev 

from lib.rvms import idfStudent,idfNormal

def welford1passRollingAVG(x,n,_sum,mean):
    diff  = x - mean
    _sum  += diff * diff * (n - 1.0) / n
    mean += diff / n
    #stdev  = sqrt(_sum / n)
    return mean,_sum

def welford1passAVG_STDEV(data):
    n,_sum,mean = 0.0,0.0,0.0
    for d in data:
        n += 1
        diff  = d - mean
        _sum  += diff * diff * (n - 1.0) / n
        mean += diff / n
    stdev  = sqrt(_sum / n)
    return mean,stdev

def confidentIntervalAVG_Student(data,loc=.95,welford1passStats=False):
    assert len(data) > 1
    if welford1passStats:   avg,stddev = welford1passAVG_STDEV(data)
    else:                   avg,stddev = mean(data),stdev(data)
    u = 1.0 - 0.5 * (1.0 - loc)                      # interval parameter  */
    t = idfStudent(len(data) - 1, u)                 # critical value of t */
    w = t * stddev / sqrt(len(data) - 1)              # interval half width */
    return avg-w,avg+w 

def getSizeForCustomConfIntervalW(data,loc=.95,w_proportionDev=.1): #algo 8.1.2 
    alpha = 1-loc
    t = idfNormal(0.0,1.0,1 - alpha/2)   #t*_+inf
    w = stdev(data) * w_proportionDev
    n      = 1
    v,xAvg = 0,data[0]
    while ( n < len(data) and (n<40 or (t*sqrt(v/n) > w*sqrt(n-1)) )):
        x =  data[n]
        n += 1 
        d =  x -xAvg
        v += d * d * (n-1)/n
        xAvg += d/n
    assert xAvg == mean(data[:n])
    return n

def getSizeForCustomConfIntervalWOnTheFlight(getData,loc=.95,w_proportionDev=.2):
    alpha = 1-loc
    t = idfNormal(0.0,1.0,1 - alpha/2)   #t*_+inf
    w = stdev(data) * w_proportionDev
    n      = 1
    v,xAvg = 0,getData()
    while ( x != None and (n<40 or (t*sqrt(v/n) > w*sqrt(n-1)) )):
        x =  getData()
        n += 1 
        d =  x -xAvg
        v += d * d * (n-1)/n
        xAvg += d/n
    assert xAvg == mean(data[:n])
    return n

if __name__ == "__main__":
    data = list(range(100,1000))
    avg,stdev = mean(data),stdev(data)
    avgW,stdevW=welford1passAVG_STDEV(data)
    assert abs(avg-avgW) < .01,     "welford1passAVG    error "
    assert abs(stdev-stdevW) < .01, "welford1passSTDEV  error"
