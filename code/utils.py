from os import environ as env 
from sys import stderr
from math import factorial
from matplotlib import pyplot
from collections import namedtuple 

from config import * 


#event diarmed => <0 (keep last event time)
isEnabled = lambda event:    event.t > 0   
def min_argMin(l): #get both min and minIdx in list @l, INFTY if it's < 0 
    try:    out=min((val, idx) for (idx, val) in enumerate(l) if isEnabled(val))
    except: out=(INFTY,None)
    return  out
def min_argMinEvent(l): #get min and minIdx in eventList @l, INFTY if it's < 0 
    try:    out=min((e.t,idx,e) for (idx, e) in enumerate(l) if isEnabled(e))
    except ValueError: out=(l[0].t,0,l[0])
    return  out

def max_argMax(l): #get both min and minIdx in list @l, INFTY if it's < 0 
    try:    out=max((val, idx) for (idx, val) in enumerate(l))
    except: out=(-INFTY,None)
    return  out
def objToNamedTup(o):   #trasform object to a named tuple with the same fields names
    items = o.__dict__.items()
    ntup  = namedtuple("tuplized",[x[0] for x in items])
    return ntup( [ x[1] for x in items ] )
    
###### SystemRandom
from random import SystemRandom
_r=SystemRandom()
sysRandom = lambda: _r.random()

#######################  GRAPH UTILS  ########################################
def plotInit(plotSize,title,labelx,labely):
    out=pyplot.figure(figsize=plotSize)
    pyplot.title(title)
    pyplot.xlabel(labelx)
    pyplot.ylabel(labely)
    return out

def plotAddYTicks(ticks):    
    oldTicks = pyplot.yticks()[0]
    for t in ticks:
        if t not in oldTicks:   pyplot.yticks(list(oldTicks)+ticks); return

def plot(dataX,dataY,plotSize=(20,10),labelx="X",labely="Y",title="PLOT",\
    reinit=True,dataLabel=None,show=True,showLegend=False):
    """ plot the given list of tuple (x,y) in @data """
    if reinit:  plotInit(plotSize,title,labelx,labely)     #init new plot
    pyplot.plot(dataX,dataY,label=dataLabel)
    if showLegend:          pyplot.legend(loc=LEGEND_LOC)
    if show:                pyplot.show()
    return pyplot
def steamPlot(data,plotSize=(20,10),labelx="X",labely="Y",title="PLOT"):
    """ plot a not connected dot of pairs(j+1,@data[j]) 
    where j is index of @data and @data is a list of floats
    steam plot as in https://matplotlib.org/devdocs/_images/sphx_glr_stem_plot_001.png """
    
    plotInit(plotSize,title,labelx,labely)     #init new plot
    pyplot.stem([j+1 for j in range(len(data))],data)
    plotAddYTicks([data[0]])
    pyplot.show()
    #if SAVE_FIG:    pyplot.savefig(cleanStr(title)+".svg")
def plotMultiOverlayWrap(dataX,title,dataYList,labelList,labelX="simulation duration-jobs"):
    assert len(dataYList) == len(labelList)
    for i,dataY in enumerate(dataYList):
        plot(dataX,dataY,dataLabel=labelList[i],title=title,show=False,reinit=i==0)
    xTicks=[j for i,j in enumerate(dataX) if i%XTICK_SUBSAMPLE_STEADYS==0]
    pyplot.xticks(xTicks,rotation="vertical");plotAddYTicks([dataYList[0][0]])
    pyplot.legend(loc=LEGEND_LOC)
    if SAVE_FIG:    pyplot.savefig(cleanStr(title)+".svg")
    pyplot.show()
    return pyplot
    
#######################  FORMULAS UTILS  #####################################
def erlangC(c,p):
    pQ_part = ( (c*p)**c ) / (factorial(c) * (1-p))
    p0 =  sum( (((c*p)**i) /  factorial(i)) for i in range(c) ) + pQ_part
    return pQ_part / p0
def msqSteadyTQ(c,p,l):
    pBlock = erlangC(c,p)
    return pBlock,(pBlock * (p/(1-p)))/l
def ssqAvgTSClassic(mu,l):              return 1/(mu-l)
def ssqAvgTS(avgService,avgInterArr):   
    return (avgService*avgInterArr) / (avgInterArr - avgService)
#######################  DEBUG UTILS  ##########################################
from collections.abc import Iterable
isIterable=lambda o:   isinstance(o, Iterable)
def unpackDictAttrRecursive(o):
    #TODO basic type not unpackable cases here 
    if (not isIterable(o) or type(o)==str or type(o)==dict) \
         and not hasattr(o,"__dict__"):  return o 
    #unpack basic iterable
    if isIterable(o):
        for i in range(len(o)):  #unpack elements
            o[i] = unpackDictAttrRecursive(o[i])
        return o 

    if hasattr(o,"__dict__"):  
        o = o.__dict__
        out=dict()
        for k,v in o.items():
            try:
                out[k] = unpackDictAttrRecursive(v)
            except: pass
        return out

def inPrint(n,interval):
    isIn=interval[0]<=n and n<=interval[1]
    isInStr=str(isIn)+"  "
    if UTF8_MATH_IN_PRINT:
        isInStr=chr(MATH_IN_ORD+1)
        if isIn:    isInStr=chr(MATH_IN_ORD)
    if isIn:    isInStr="\33[91m\33[1m\33[103m%s\33[0m"%isInStr
    intLen    = interval[1] - interval[0]
    centering = .5-(n-interval[0])/(intLen)
    out = "%11f" % float(n)+"\t"+isInStr+str(interval).ljust(45)+" len:%6f"%intLen
    if isIn:    out+=" .5_centering %f"%abs(centering)
    return out

def smartP(o,header=""):
    print("\n\n",header,sep="")
    if type(o) == list:
        for i,x in enumerate(o):
            print("entry:",i)
            if type(x) == dict: 
                for k,v in x.items():   print("",k,v)
    elif type(o) == dict:
        for k,v in o.items():   print(k,v)
    else:   print(o)
    return ""
def printFloatsShorted(l):
    for x in l: print("%1.4f" % x,end="\t")
    print("")
    return ""
def printVars(varsD=None,mod=None,unpackDictAttr=True,filter__=True):
    """
    get variables from @mod (e.g. sys.modules["__main__"])
    otherwise use the variables dictionary passed in @varsD != None
    then filter the vars with __ in name if @filter__ 
    unpack __dict__ attribute in all variables having it if @unpackDictAttr
    :Returns filtered,unpacked variables and print them 
    """
    filterUseless = lambda : True
    if filter__ :   filterUseless = lambda x,y: "__" not in x
    varsModOrigin = []
    if mod != None: varsModOrigin = [mod]
    if varsD != None:   v = list(varsD.items())
    else:
        v = [[x,y] for x,y in vars(*varsModOrigin).items() \
            if not filterUseless(x,y)]
    if unpackDictAttr: 
        for x in range(len(v)):
            if DEBUG:   print("unpacking var:",v[x],file=stderr);#breakpoint()
            try:    v[x]=(v[x][0], unpackDictAttrRecursive(v[x][1]))
            except: print("error at",v[x],file=stderr)
    #print unpacked,filtered vars 
    v.sort()
    print("\n\n\n")
    for vName,vVal in v:
        if "sampled" in vName or "waits" in vName:continue #skip huge field...
        print("%12s\t" % vName,vVal,end="\n\n",sep="")
    return v
def p(o):   print(o)
def po(o):  print(o.__dict__)
def cleanStr(s):
    #pathname safe cleaning of str @s
    CLEAN_CHRS=[" ","_","/"]
    for c in CLEAN_CHRS: s=s.replace(c,"_")
    return s
fullCopy      = lambda o,_=None:    o
HIGHLIGHTED="\33[91m\33[1m\33[103m%s\33[0m"
#LOCAL TEST
if __name__ == "__main__":  
    #printVars()
    #print(erlangC(2,0.456107))
    print(msqSteadyTQ(4,.5/(4/1.66),.5))
    print(ssqAvgTSClassic(0.008266858152976555,0.003404306709763072))
