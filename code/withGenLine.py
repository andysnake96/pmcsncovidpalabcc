"""
N.E. simul of PalaBCC vaccination center: 
MultiServerQueue => 6xSSQ (vaccination lines) organized in 2 kinds: rna, std

MSQ:The service node is assumed to be initially idle, no arrivals are permitted after the terminal STOP (time || createdJobs) and the node is then  purged by processing any remaining jobs.
CONNECTION:completition in the msq will correspond to scheduled arrivials in the ssq 
SSQ: ... 
tipi di eventi: arrivo + m completamenti in MSQ + arrivo/accodamento in SSQ + completamento in SSQ
"""

##import from .. 
#import sys, os
#sys.path.insert(0, os.path.dirname(__file__)+"/..")

#srcMod deps
from lib.rngs     import plantSeeds, random, selectStream,getSeed,stream
from lib.estimate import *
from lib.rvgs     import *

from entity_withGenLine import *
from config       import * 
from utils        import  *

from copy         import deepcopy
from sys          import argv
from statistics   import mean,variance
from time         import perf_counter

def GetArrival(arrivF=lambda p: Exponential(p),l=IN_LAMBDA**-1):
    global ArrivalTemp

    selectStream(ARRIVAL_STREAM) 
    ArrivalTemp += arrivF(l)
    return ArrivalTemp

_EXP = lambda p:  lambda: Exponential(p)
EXP = lambda p=2: Exponential(p)
def GetService(serviceF=EXP, stream=SERVICE_STREAM_ACCEPT):
  selectStream(stream)
  return serviceF()

### AUX
CountLines = None           #consistety check _getVaccLine
LastIdxRna,LastIdxStd = 0,0 #_getVaccLineRR

def _getVaccLineRR(events):
    global LastIdxRna,LastIdxStd 
    #pick vacc line on Prob vaccKind and round robin over the vaccType lines
    selectStream(PROUTING_STREAM)
    r = random()    #random route
    out = None
    if r<=P_RNA: 
        LastIdxRna = (LastIdxRna + 1) % len(events.rnaSSQ) 
        CountLines[LastIdxRna] += 1
        out = events.rnaSSQ[LastIdxRna]
    else:
        LastIdxStd = (LastIdxStd + 1) % len(events.stdSSQ)
        kindStartIdx = len(events.rnaSSQ)
        CountLines[kindStartIdx + LastIdxStd] += 1
        out = events.stdSSQ[LastIdxStd]
    return out

def _getVaccLineShortestQ(events,states):
    #pick vacc line on Prob vaccKind and round robin over the vaccType lines
    selectStream(PROUTING_STREAM)
    r = random()    #random route
    out = None
    if r<=P_RNA: 
        trgtLine = min((len(ssq.jIn),idx) for idx,ssq in enumerate(states.rnaSSQ))[1]
        CountLines[trgtLine] += 1
        out = events.rnaSSQ[trgtLine]
    else:
        trgtLine = min((len(ssq.jIn),idx) for idx,ssq in enumerate(states.stdSSQ))[1]
        CountLines[trgtLine+len(events.rnaSSQ)] += 1
        out = events.stdSSQ[trgtLine]
    return out

def _getVaccLineFullRnd(events,rnaRangesEnd,stdRangeEnd): 
    #equilikelly choice on precomputed probRangeEnds on 2 kind of events
    selectStream(PROUTING_STREAM)
    r = random()
    for i in range(len(rnaRangesEnd)):
        if r < rnaRangesEnd[i]:  
            CountLines[i] += 1 
            return events.rnaSSQ[i]
    for i in range(len(stdRangeEnd)):
        if r < stdRangeEnd[i]:   
            CountLines[i + len(rnaRangesEnd) ] += 1 
            return events.stdSSQ[i]
    assert False,"SHOULDNT BE HERE"
def _mean(data):
    out=None
    try:    out=mean(data)
    except: out=0
    return out
def _variance(data):
    out=None
    try:    out=variance(data)
    except: out=0
    return out
def _msqAcceptanceMetrics(msqEvents,msqState,t):
    out={"index" : msqState.index}
    out["avgInterrarivals"]     = t.curr / msqState.index
    out["avg#InNode"]           = msqState.area / t.curr  #TH VAL MEDIO
    #queue any: adjust areaMSQ to calculate queue averages 
    areaQueue                   = msqState.getQueueArea()
    out["avg#InQueue"]          = areaQueue / t.curr
    out["avgDelayLittle"]       = areaQueue / msqState.index
    #(N_avg*t)/n_tot = N_avg / lambda =_little_= T_s
    out["avgWaitLittle"]        = msqState.area / msqState.index   
    delays,waits=[x[0] for x in msqState.waits],[x[1] for x in msqState.waits]
    out["avgWait"]              = ( mean(waits)  , variance(waits)  )
    out["avgDelay"]             = ( mean(delays) , variance(delays) )
    _sStats = lambda s: {\
        "utilization":s.service / t.curr,"avgService":s.service / s.served, \
        "share": float(s.served) / msqState.index }
    out["servers"]     = [_sStats(s) for s in msqState.servers]
    out["utilizationAvg"]     = mean([x["utilization"] for x in out["servers"]])
    if not AUDIT:   return out
    #print also
    print("for %d jobs the service node statistics are:\n" % out["index"])
    print("\tavg interarrivals    = %f" % out["avgInterrarivals"])
    print("\tavg # in node        = %f" % out["avg#InNode"])
    print("\tavg # in queue       = %f" % out["avg#InQueue"])
    print("\tavg P(queue)         = %f" % (areaQueue/msqState.area))
    print("\tavg delayLittle      = %f" % out["avgDelayLittle"])
    print("\tavg waitLittle       = %f" % out["avgWaitLittle"])
    print("\tavg delay            = avg:%f var:%f" % out["avgDelay"])
    print("\tavg wait             = avg:%f var:%f" % out["avgWait"])
    print("\nSERVER STATS")
    for sId,s in enumerate(out["servers"]):
        print("\tid: %d utilization: %f avgService: %f share: %f" % \
          (sId,s["utilization"],s["avgService"],s["share"]))
    print("\tavg utilization      = %f " % out["utilizationAvg"])
    print("\nsteady stats")
    p,pBlock,steadyMSQ_TQ,steadyMSQ_TS,steadyMSQ_NQ,steadyMSQ_N = msqSteady(MSQ_SERVERS_N)
    print("\tlambda\temp:",1/out["avgInterrarivals"],"steady:",IN_LAMBDA,"mu_i:",\
        1/E_TS_I_ACCEPT,"serversN:",MSQ_SERVERS_N)
    return out


def _ssqVaccinationLine(ssqEvents,ssqState,t):
    if ssqState.index == 0:   raise Exception("empty ssq vaccLine")
    out={"index" : ssqState.index}
    #out["avgInterrarivals"]  = -ssqEvents.arrival / ssqState.index#TODO TODO TODO
    out["avgInterrarivals"]     = t.curr / ssqState.index 
    out["avgService"]           = ssqState.areaService / ssqState.index
    areaQueueSSQ                = ssqState.areaFull - ssqState.areaService 
    #out["avgDelayLittle"]     = areaQueueSSQ / ssqState.index
    out["avgDelayLittle"]       = ssqState.getDelay()
    ## int{n(t)} / #arrivi  = n_avg * T_tot / #arrivi = n_avg / lambda =_little_=E[Ts]
    #out["avgWaitLittle"]     = ssqState.areaFull / ssqState.index 
    out["avgWaitLittle"]     = ssqState.getWait()
    delays,waits=[x[0] for x in ssqState.waits],[x[1] for x in ssqState.waits]
    out["avgWait"]              = ( mean(waits), _variance(waits) )
    out["avgDelay"]             = ( mean(delays),_variance(delays) )
    out["avg#InNode"]           = ssqState.areaFull / t.curr
    out["avg#InQueue"]          = areaQueueSSQ / t.curr
    out["utilization"]          = ssqState.areaService/ t.curr
    if not AUDIT:   return out
    #print also
    print("\nfor %d jobs"                     % out["index"])
    print("\tavg interarrival time     = %f"  % out["avgInterrarivals"]) 
    print("\taverage service time      = %f"  % out["avgService"])
    print("\taverage delay time Little = %f"  % out["avgDelayLittle"])
    print("\taverage wait  time Little = %f"  % out["avgWaitLittle"])
    print("\taverage delay time        = avg:%f var:%f"  % out["avgDelay"])
    print("\taverage wait  time        = avg:%f var:%f"  % out["avgWait"])
    
    print("\taverage # in the node     = %f"  % out["avg#InNode"])
    print("\taverage # in the queue    = %f"  % out["avg#InQueue"])
    print("\tutilization               = %f"  % out["utilization"])
    #Steady prints 
    msqAcceptancePblock = erlangC(MSQ_SERVERS_N, (IN_LAMBDA * E_TS_I_ACCEPT) /MSQ_SERVERS_N)
    l,mu,e_tq,e_ts = ssqSteady(ssqEvents.kind,IN_LAMBDA)
    print("Emp:\tlambda:",1/out["avgInterrarivals"],"mu:",1/out["avgService"])
    return out




    
_getWaits      = lambda s,_=None:    s.getWaits()
getPopol      = lambda s,_=None:     s.getPopolations()
getThourghput = lambda s,tCurr:     sum(x.index for x in s.allSSQ)/tCurr
_mainSysStat   = namedtuple("mainSysStat","sysWait sysPopolation sysThroughput")
getMainSysStat= lambda s,tCurr:    _mainSysStat(s.getWaits(),\
                s.getPopolations(tCurr),sum(x.index for x in s.allSSQ)/tCurr)

def _smartCopy(state,statExtr=_getWaits,tCurr=None,deepCp=False):
    #extract target stats for each node via @statExtr[dflt full copy]
    if statExtr == None:    return None
    if deepCp:
        out = deepcopy(statExtr(state,tCurr))
        #tmp = state.MSQAcceptance.jInQueue
        #state.MSQAcceptance.jInQueue = len(tmp)
        #state.MSQAcceptance.jInQueue = tmp           #copy back jIn
    else:       out = statExtr(state,tCurr)
    return out

######################################################################
def main(seed=SEED,stop=STOP,sample_stats=SAMPLE_STATS,\
         statExtr=_getWaits,runtimeSampling=RUNTIMESAMPLING):
    """
    simulation program of covid vaccination center
    arrival/service generation based on the given @seed
     if @seed == None, no new seed will be setted (see transientReplicaAny)
     initial seed (quick dflt) manually set if @seed=0
    The simulation will endure for @stop ( seconds || createdJobs )
    system state will be sampled each @sample_stats ( seconds || jobs on config)
        because deepCopy recursion, acceptance jobs arrivals is replaced with 
        number of jobs in acceptance (queue + inService)
    
    Returns:
        dictionaries:
        -simulation produced statistics on the accptance,
        -list of simulation produced statistics on vaccination lines
        -global system statistics

        -sampled sys state smart deepCopied like [( (time,jobsGenerated),trgtStats )]

    """
    global CountLines,ArrivalTemp
    if len(argv) > 1 and  "-h" in argv[1].lower():  print(__doc__,ENV_CONFIG_HELP);exit(1)

    if seed != None:    plantSeeds(seed)
    start = perf_counter()
    #OUTs vars
    accStats,vaccStats,sysStats = dict(),list(),dict()
    sampledStates = list()      #[( (time,jobsGenerated),trgtStats )]
    ##INIT
    ArrivalTemp = START
    t             = Time()
    events        = Events()
    state         = State()
    acceptance    = state.MSQAcceptance     #quick ref 
    events.MSQAcceptance.arrival = Event(GetArrival())    #first arrival 
    CountLines    = [0] * len(state.allSSQ)
    #AUX
    oldT          = t.curr
    #TODO ONLY IN FULL PROB VACCLINE SELECT MODE
    pRnaLine      = P_RNA / len(events.rnaSSQ)
    rnaRangesEnd  = [ (1+x)*pRnaLine for x in range(len(events.rnaSSQ)) ]
    pStdLine      = P_STD / len(events.stdSSQ)
    stdRangesEnd  = [ P_RNA+(1+x)*pStdLine for x in range(len(events.stdSSQ)) ]
    #stat gather
    _acceptStats   = lambda: _msqAcceptanceMetrics(events.MSQAcceptance,acceptance,t)
    _rnaStats      = lambda: [ _ssqVaccinationLine(e,s,t) for e,s in \
        [ (events.rnaSSQ[x],state.rnaSSQ[x]) for x in range(len(state.rnaSSQ)) ]]
    _stdStats      = lambda: [ _ssqVaccinationLine(e,s,t) for e,s in \
        [ (events.stdSSQ[x],state.stdSSQ[x]) for x in range(len(state.stdSSQ)) ]]
    oldGeneratedJobsN,oldCompleatedJobsN=0,0

    ########### CORE ## PROCESS NEW ARRIVIAL OR PREV. QUEUED    ################
    while events.isAnyEnabled() or state.existsWork():
        ##TODO DEBUG with open("/tmp/msq_net.log","w") as f: f.write("%d\t%d\n"%(t.next,len(state.jInMSQAcceptance)))
        e,eType,eIdx = events.getNextEvent()
        t.next = e.t     # next event time
        deltaT = t.next - t.curr 
        oldT   = t.curr
        t.curr = t.next    # ADVANCE THE CLOCK
        
        if runtimeSampling and ( \
          (SAMPLING_MODE=="TIME"    and (t.curr - oldTSampl) >= sample_stats) or\
          (SAMPLING_MODE=="JOBS_IN" and (t.generatedJobsN-oldGeneratedJobsN)>=sample_stats)):
            sampledStates.append( \
             ((oldT,t.generatedJobsN),_smartCopy(state,statExtr,t.curr)) )
            oldTSampl = t.curr
            oldGeneratedJobsN=t.generatedJobsN

        ##cumulatve integral n(t) updates
        acceptance.area += deltaT * len(acceptance)
            
        for ssq in state.allSSQG:    
            ssq.areaFull += deltaT * len(ssq.jIn)
            if len(ssq.jIn) > 0:    ssq.areaService += deltaT

        ######## EVENTs PROCESSING ##########################################
        ### MSQ -- NETWORK START NODE 
        if   eType == "arrAccept":
            t.generatedJobsN += 1
            nnode = acceptance.jInQueue.append((t.curr,e)) #enqueue arrival
            lastArrival = events.MSQAcceptance.arrival
            events.MSQAcceptance.arrival = Event(GetArrival())    #next arrival
            #disarm if end reached
            if (STOP_MODE == "TIME"     and events.MSQAcceptance.arrival.t > stop) or\
               (STOP_MODE == "JOBS_IN"  and t.generatedJobsN > stop) or\
               (STOP_MODE == "JOBS_OUT" and t.compleatedJobsN> stop):
                events.MSQAcceptance.arrival.t = -lastArrival.t
                if AUDIT:   print("Closing doors:\t",t.__dict__)
            if len(acceptance) <= MSQ_SERVERS_N:   #start processing
                s        = events.MSQAcceptance.findAvailServer()
                service  = GetService(serviceF=_EXP(E_TS_I_ACCEPT),\
                                      stream = SERVICE_STREAM_ACCEPT+s)
                #SERVER STATS UPDATE
                acceptance.servers[s].service += service
                acceptance.servers[s].served  += 1
                #SCHEDULE EVENT AT SERVER s
                arrTime,toStartEvent = acceptance.jInQueue.pop(0)
                assert e == toStartEvent
                e.t = t.curr+service
                events.MSQAcceptance.completions[s] = e
                acceptance.addWait( (0,service) )
                acceptance.jInService += 1
        elif eType == "compAccept":      
            s_msq  = eIdx
            acceptance.index        += 1
            acceptance.jInService   -= 1 
            #route to vaccination line
            if   VACC_LINE_MODE=="RND":       ssqLine = _getVaccLineFullRnd(events,rnaRangesEnd,stdRangesEnd)
            elif VACC_LINE_MODE=="SHORTESTQ": ssqLine = _getVaccLineShortestQ(events,state)
            elif VACC_LINE_MODE=="RR":        ssqLine = _getVaccLineRR(events)
            minQLen = min((len(ssq.jIn),idx) for idx,ssq in enumerate(state.allSSQ))[1]
            if minQLen   > 4 and ssqLine.kind == STD: ssqLine=events.genSSQ;print("a",end="")
            elif minQLen > 3 and ssqLine.kind == RNA: ssqLine=events.genSSQ;print("b",end="")
            e.t = t.curr
            ssqLine.arrival = e    #goto vaccLine as an arrival

            ##still exists queue => schedule a new depart in the freed server
            if len(acceptance) >= MSQ_SERVERS_N:
                service = GetService(serviceF=_EXP(E_TS_I_ACCEPT)\
                    ,stream=SERVICE_STREAM_ACCEPT+s_msq)
                toStartArrival,toStartEvent = acceptance.jInQueue.pop(0)
                acceptance.jInService += 1 
                queue = t.curr - toStartArrival
                acceptance.addWait( (queue,queue+service) )
                toStartEvent.t = t.curr+service
                events.MSQAcceptance.completions[s_msq]=toStartEvent
                
                #if mean([x[0] for x in acceptance.waits]) > 33 or queue > 100: breakpoint()

                acceptance.servers[s_msq].service  += service
                acceptance.servers[s_msq].served   += 1 
      
            else:    #nothingTodo=>disarm 
                _ended = deepcopy(events.MSQAcceptance.completions[s_msq])
                _ended.t *= -1 
                events.MSQAcceptance.completions[s_msq] = _ended
        ### SSQ after MSQ 
        elif eType in [ "arrSSQrna" , "arrSSQstd","arrSSQGen" ]:
            trgtEvL,trgtStL,trgtStreamL=events.rnaSSQ,state.rnaSSQ,SERVICE_STREAM_RNA
            if eType == "arrSSQstd":
                trgtEvL,trgtStL,trgtStreamL=events.stdSSQ,state.stdSSQ,SERVICE_STREAM_STD
            ssqEvents,ssqState,streamIdx=trgtEvL[eIdx],trgtStL[eIdx],trgtStreamL[eIdx]
            
            if eType == "arrSSQGen":
                ssqEvents,ssqState,streamIdx=events.genSSQ,state.genSSQ,SERVICE_STREAM_GEN
            ssqState.jIn.append((t.curr,e))
            _e = deepcopy(ssqEvents.arrival); _e.t *=-1
            ssqEvents.arrival = _e
            if len(ssqState.jIn) == 1: ###PRIMA CENTRO VUOTO => SCHEDULA PROCESSAMENTO
                serviceF = _EXP(E_TS_I_STD)
                if  ssqEvents.kind == RNA:    serviceF = _EXP(E_TS_I_RNA)
                elif  ssqEvents.kind =="GEN":   serviceF = _EXP(E_TS_I_GEN)
                service  = GetService(serviceF=serviceF,stream = streamIdx)
                assert e == ssqState.jIn[0][1]
                e.t = t.curr + service
                ssqEvents.completion = e
                ssqState.addWait( (0,service) )

        elif eType in [ "compSSQrna" , "compSSQstd","compSSQGen" ]:
            trgtEvL,trgtStL,trgtStreamL=events.rnaSSQ,state.rnaSSQ,SERVICE_STREAM_RNA
            if eType == "compSSQstd":
                trgtEvL,trgtStL,trgtStreamL=events.stdSSQ,state.stdSSQ,SERVICE_STREAM_STD
            ssqEvents,ssqState,streamIdx=trgtEvL[eIdx],trgtStL[eIdx],trgtStreamL[eIdx]
            
            if eType == "compSSQGen":
                ssqEvents,ssqState,streamIdx=events.genSSQ,state.genSSQ,SERVICE_STREAM_GEN
            ssqState.index += 1
            arrTime,jobEvent = ssqState.jIn.pop(0)  #FIFO out

            t.compleatedJobsN += 1 
            if runtimeSampling and SAMPLING_MODE=="JOBS_OUT" and \
              (t.compleatedJobsN-oldCompleatedJobsN)>=sample_stats:
                state.sysJobWaits.append(t.curr - jobEvent.creation)
                sampledStates.append( \
                  ((t.curr,t.compleatedJobsN),_smartCopy(state,statExtr,t.curr)) )
                oldCompleatedJobsN = t.compleatedJobsN

            if len(ssqState.jIn) > 0: #still exists queue => schedule a new depart   
                serviceF = _EXP(E_TS_I_STD)
                if  ssqEvents.kind == RNA:    serviceF = _EXP(E_TS_I_RNA)
                elif  ssqEvents.kind =="GEN":   serviceF = _EXP(E_TS_I_GEN)
                service = GetService(serviceF=serviceF,stream = streamIdx)
                toStartArrival,toStartEvent = ssqState.jIn[0]
                toStartEvent.t = t.curr+service
                ssqEvents.completion  = toStartEvent
                queue = t.curr - toStartArrival
                ssqState.addWait( (queue,queue+service) )

            else:
                _ended = deepcopy(ssqEvents.completion)
                _ended.t *= -1 
                ssqEvents.completion = _ended
    
    endSimul = perf_counter()
    ############################################################################
    if AUDIT: print("\n################# %11s ##################" % "ACCEPTANCE")
    #accStats =_msqAcceptanceMetrics(events.MSQAcceptance,acceptance,t)
    accStats = _acceptStats()
    if AUDIT: print("\n################# %11s ##################" % "RNA LINES")
    #for ssqEv,ssqSt in [(events.rnaSSQ[x],state.rnaSSQ[x]) for x in range(len(state.rnaSSQ))]:
    #    vaccStats.append(_ssqVaccinationLine(ssqEv,ssqSt,t))
    vaccStats += _rnaStats()
    if AUDIT: print("\n################# %11s ##################" % "STD LINES")
    #for ssqEv,ssqSt in [(events.stdSSQ[x],state.stdSSQ[x]) for x in range(len(state.stdSSQ))]:
    #    vaccStats.append(_ssqVaccinationLine(ssqEv,ssqSt,t))
    vaccStats += _stdStats()
    genLineStat = _ssqVaccinationLine(events.genSSQ,state.genSSQ,t)
    
    ######### Global - Steady
    vaccLineAreaCumul = sum(x.areaFull for x in state.allSSQG)
    vaccLinesIndexCumul = sum(x.index for x in state.allSSQG)
    #indexAll = vaccLinesIndexCumul + acceptance.index
    sysStats["avgWaitSysLittle"] = (vaccLineAreaCumul+acceptance.area)/vaccLinesIndexCumul
    avgVaccLinesWaits=sum(v["avgWait"][0]*v["index"]/vaccLinesIndexCumul for v in vaccStats)
    sysStats["avgWaitSysWAvg"]   = accStats["avgWait"][0] + avgVaccLinesWaits
    if runtimeSampling: sysStats["avgWaitSysSingleJobAvg"]=mean(state.sysJobWaits)
    else:               sysStats["avgWaitSysSingleJobAvg"]=None
    sysStats["sysJobWaits"]=state.sysJobWaits   #TODO COMMODITY PUTTED HERE
    #Steady vals of nodes
    p,pBlock,steadyMSQ_TQ,steadyMSQ_TS,steadyMSQ_NQ,steadyMSQ_N,l_rna,mu_rna,e_tq_rna,\
        e_ts_rna,l_std,mu_std,e_tq_std,e_ts_std,avgWaitSysAny = steadyVals()
    sysStats["throughputSys"]=vaccLinesIndexCumul/t.curr
    throughputSysAny = IN_LAMBDA
    sysStats["avgPopolSysArea"]=(state.MSQAcceptance.area+vaccLineAreaCumul)/t.curr
    sysStats["avgPopolSysLittle_Wavg"] = sysStats["avgWaitSysWAvg"] * IN_LAMBDA 
    avgPopolSysSteady = avgWaitSysAny * IN_LAMBDA 
    if AUDIT:
        print("\n################# %11s ##################" % "SUMMARY" )
        print("avgWaitTimeSys Little-Area:",sysStats["avgWaitSysLittle"],\
         "WeightAvg:",sysStats["avgWaitSysWAvg"],"singleJob:",sysStats["avgWaitSysSingleJobAvg"],\
        "\navgWaitTimeSys Steady-Any:",avgWaitSysAny)
        print("avgPopolSys:\tArea",sysStats["avgPopolSysArea"],\
            "Little:",sysStats["avgPopolSysLittle_Wavg"],"Steady:",avgPopolSysSteady) 
        print("throughputSys:",sysStats["throughputSys"],"Steady-Analitic:",throughputSysAny)
        print("\n\nutilizations:\nacceptanceServers:",\
                     [x["utilization"] for x in accStats["servers"]],  \
            "\nrna:",[x["utilization"] for x in vaccStats[:SSQ_RNA_N]],\
            "\nstd:",[x["utilization"] for x in vaccStats[SSQ_RNA_N:]] )
        print("cfg service times:\taccept:",E_TS_I_ACCEPT,"RNA",E_TS_I_RNA,"STD",E_TS_I_STD)
        ##print("\n################# %11s ##################" % "PLOTs")
    if AUDIT: print("\n################# %11s ##################" % "CONSISTENCY" )
    probs      = [ c / acceptance.index for c in CountLines ] #probab.
    rnaProbs   = probs[:len(state.rnaSSQ)]
    stdProbs   = probs[len(state.rnaSSQ):]
    if AUDIT:
        print("rna vacc. lines probabilities:",rnaProbs)
        print("std vacc. lines probabilities:",stdProbs)
        print("CountLines all lines counters:",CountLines)
    assert abs( sum(probs) - 1 ) < FPDELTA,"not well distribuited probability for lines"
    for p in rnaProbs[1:]:  assert abs(rnaProbs[0]-p) < PDIFF,"rna prob not uniform" 
    for p in stdProbs[1:]:  assert abs(stdProbs[0]-p) < PDIFF,"std prob not uniform"  
    #NODES CHECKS
    delta=abs(accStats["avg#InNode"] - \
            (   accStats["avgWait"][0] / accStats["avgInterrarivals"] ))
    assert delta < THREASH_DELTA,"accept avg#InNode delta: %f" % delta 
    consistDeltas={"acceptLittle avg#InNode":delta}
    delta=abs(accStats["avgWait"][0] - accStats["avgWaitLittle"])
    assert delta < THREASH_DELTA,"accept waitLittle delta: %f" % delta 
    assert abs(accStats["avgWait"][0] - acceptance.waitAvgWelford) < THREASH_DELTA
    consistDeltas={"acceptLittle wait":delta}
    for i,vlStat in enumerate(vaccStats):
        delta=abs(vlStat["avgWait"][0] - vlStat["avgWaitLittle"])
        assert delta < THREASH_DELTA,"vaccLine waitLittle %d:%f" % (i,delta)
        consistDeltas["vaccLine%d waitLittle" % i]=delta
        delta=abs(vlStat["avg#InNode"] - \
            (  accStats["avgWait"][0] / vlStat["avgInterrarivals"]  ))
        #assert delta < THREASH_DELTA,"vaccLine avg#InNodeLittle %d:%f\t,utiliz:%f" % (i,delta,vlStat["utilization"])
        consistDeltas["vaccLine%d Avg#InNodeLittle" % i]=delta
        assert abs(vlStat["avgWait"][0]-state.allSSQ[i].waitAvgWelford)<THREASH_DELTA
    #SYS CHECK
    assert acceptance.index == vaccLinesIndexCumul,"jobs lost!"
    if runtimeSampling and sample_stats==1:
        assert abs(sysStats["avgWaitSysLittle"]-sysStats["avgWaitSysSingleJobAvg"])<THREASH_DELTA
    assert abs(sysStats["avgWaitSysLittle"] - sysStats["avgWaitSysWAvg"]) < THREASH_DELTA
    #assert abs(sysStats["avgPopolSysArea"] - sysStats["avgPopolSysLittle_Wavg"])<.5
    if DEBUG:
        print("\nconsistDeltas")
        for k,v in consistDeltas.items():   print(k.ljust(30),v)
    #processed element num cumulative matching

    end = perf_counter()
    if DEBUG:
        print("\n################# %11s    ##################" % "DEBUG" )
        printVars(locals())
        printVars({k:v for k,v in globals().items() if type(k) == str and k.isupper()})
    if AUDIT:
        print("\n\nSTOP STATE: time",t.curr,"GENERATED JOBS",t.generatedJobsN)
        print("elapsed:",endSimul-start,"stats&checks [dbg]:",end-endSimul,end="\n\n")
    sysStats["stopTime"],sysStats["stopJobsN"] = t.curr,t.generatedJobsN
    return accStats,vaccStats,sysStats,sampledStates

def msqSteady(nServer,l=IN_LAMBDA,es_i=E_TS_I_ACCEPT):   
    p = (l * es_i) / ( nServer )
    pBlock,steadyMSQ_TQ = msqSteadyTQ(nServer,p,l)
    steadyMSQ_TS = steadyMSQ_TQ + es_i 
    steadyMSQ_NQ,steadyMSQ_N = l*steadyMSQ_TQ,l*steadyMSQ_TS
    if AUDIT:  print("\tp_glob=%f\tE[T_Q]=%f\tE[T_S]=%f\tE[N_Q]=%f\tE[N]=%f\tpBlock=%f"%\
                (p,steadyMSQ_TQ,steadyMSQ_TS,steadyMSQ_NQ,steadyMSQ_N,pBlock) )
    return p,pBlock,steadyMSQ_TQ,steadyMSQ_TS,steadyMSQ_NQ,steadyMSQ_N
def ssqSteady(kind,srcLambda):    #Returns steady lambda,mu,E[Tq],E[Ts]
    e_s,pLineGroup,groupSize = E_TS_I_STD,P_STD,SSQ_STD_N
    if kind == RNA:   e_s,pLineGroup,groupSize = E_TS_I_RNA,P_RNA,SSQ_RNA_N
    l = (srcLambda*pLineGroup)/groupSize
    assert l <= (1/e_s)
    e_ts = ssqAvgTS(e_s,1/l)
    e_tq = e_ts - e_s
    if AUDIT:
        print("Steady SSQ vaccLine:%s\tlambda:%f\tmu:%f->p=%f\t->\tE[T_Q]:%f\tE[T_S]:%f"\
        % (kind,l,1/e_s,l*e_s,e_tq,e_ts) )
    return l,1/e_s,e_tq,e_ts
def steadyVals():
    p,pBlock,steadyMSQ_TQ,steadyMSQ_TS,steadyMSQ_NQ,steadyMSQ_N = msqSteady(MSQ_SERVERS_N)
    l_rna,mu_rna,e_tq_rna,e_ts_rna = ssqSteady(RNA,IN_LAMBDA)
    l_std,mu_std,e_tq_std,e_ts_std = ssqSteady(STD,IN_LAMBDA)
    avgWaitSysAny = steadyMSQ_TS + e_ts_rna*P_RNA + e_ts_std*P_STD 

    return p,pBlock,steadyMSQ_TQ,steadyMSQ_TS,steadyMSQ_NQ,steadyMSQ_N, \
      l_rna,mu_rna,e_tq_rna,e_ts_rna,l_std,mu_std,e_tq_std,e_ts_std,avgWaitSysAny

if __name__ == "__main__":
    #AUDIT,DEBUG = True,True
    main();exit(0)
    
