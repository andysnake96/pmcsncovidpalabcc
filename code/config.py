from os import environ as env 

START               = 0.0            # initial (open the door)   time
STOP                = float(env.get("STOP",3e3))
STOP_STEADY_SEARCH  = float(env.get("STOP_STEADY_SEARCH",2e3)) 
STOP_TRANSIENT_ANY  = float(env.get("STOP_TRANSIENT_ANY",7e2)) 
STOP_MODE           = env.get("STOP_MODE","JOBS_IN")
VALIDATION_EARLY_BREAK = False  #TODO ONLY FOR VALIDATION GRAPHS
SAMPLING_MODE       = env.get("SAMPLING_MODE","JOBS_OUT")
modes = ["TIME","JOBS_IN","JOBS_OUT"]
assert STOP_MODE     in modes,"%s either %s" % (STOP_MODE,"||".join(modes))
assert SAMPLING_MODE in modes,"%s either %s" % (STOP_MODE,"||".join(modes))
SAMPLE_STATS    = int(env.get("SAMPLE_STATS",1)) 
RUNTIMESAMPLING = True     #during main sample states every SAMPLE_STATS(jobs || secs)
if "F" in env.get("RUNTIMESAMPLING","T").upper():   RUNTIMESAMPLING = False
#BATCHMEANS
K_BATCH_N       = int(env.get("K_BATCH_N",64))
B_BATCH_S       = int(env.get("B_BATCH_S",100))
#system configuration
MSQ_SERVERS_N   = 2
SSQ_RNA_N       = 2
SSQ_STD_N       = 4

GEN_SSQ_POLICY_THREASH = 3
#vaccine kind probability
P_RNA           = .4
P_STD           = 1-P_RNA 
#rates config
IN_LAMBDA       = 700/(8.5*3600)          #j/s
_AVG_T_RNA_TALK = 110
_AVG_T_STD_TALK = _AVG_T_RNA_TALK * 1.4
_AVG_T_TALK     = _AVG_T_RNA_TALK * P_RNA + _AVG_T_STD_TALK * P_STD
_AVG_T_INJECT   = 65
E_TS_I_ACCEPT   = 30
E_TS_I_RNA      = _AVG_T_INJECT + _AVG_T_RNA_TALK
E_TS_I_STD      = _AVG_T_INJECT + _AVG_T_RNA_TALK * 1.4
E_TS_I_GEN      = _AVG_T_INJECT + _AVG_T_TALK
VACC_LINE_MODE  = env.get("VACC_LINE_MODE","RND")
assert VACC_LINE_MODE in ["SHORTESTQ","RR","RND"]

NREPL           = int(env.get("NREPL",44))
TAILOR_NREPL_PER_FIELD = True
if "F" in env.get("TAILOR_NREPL_PER_FIELD","T").upper(): TAILOR_NREPL_PER_FIELD = False
LastNNRoutingIdx,STREAM_SEPARATION = 0,1
ARRIVAL_STREAM          = LastNNRoutingIdx; LastNNRoutingIdx += STREAM_SEPARATION
SERVICE_STREAM_ACCEPT   = LastNNRoutingIdx; LastNNRoutingIdx += MSQ_SERVERS_N*STREAM_SEPARATION
####### TODO 1 stream per node
SERVICE_STREAM_RNA      = [LastNNRoutingIdx+i for i in range(SSQ_RNA_N)]; LastNNRoutingIdx += SSQ_RNA_N*STREAM_SEPARATION
SERVICE_STREAM_STD      = [LastNNRoutingIdx+i for i in range(SSQ_STD_N)]; LastNNRoutingIdx += SSQ_STD_N*STREAM_SEPARATION
SERVICE_STREAM_GEN      = LastNNRoutingIdx;      LastNNRoutingIdx+=STREAM_SEPARATION
PROUTING_STREAM         = -STREAM_SEPARATION % 256 
STREAMS=[("ARRIVAL_STREAM",ARRIVAL_STREAM),("SERVICE_STREAM_ACCEPT",SERVICE_STREAM_ACCEPT),("PROUTING_STREAM",PROUTING_STREAM)]
STREAMS+=[("SERVICE_STREAM_RNA_%d"%i,SERVICE_STREAM_RNA[i]) for i in range(len(SERVICE_STREAM_RNA))] 
STREAMS+=[("SERVICE_STREAM_STD_%d"%i,SERVICE_STREAM_STD[i]) for i in range(len(SERVICE_STREAM_STD))] 
#######
FIND_IDLE_MOST_SERVER=True #in msq, return the server-event idx of the the server idle since most time 
DFLT_SEED       = int(env.get("DFLT_SEED",123456789))  #dflt seed
SEED            = int(env.get("SEED",0))               #dflt seed for plantSeeds
THREASH_DELTA   = 1e-1  #TODO TO MUCH????
FPDELTA         = 1e-6
PDIFF           = 1.5e-1

LOC             = .95
##PLOT
SAVE_FIG=True
XTICK_SUBSAMPLE_STEADYS = 1
LEGEND_LOC = "best" #"upper left"
#NODE OUT STAT NAMES
N_SYS        = "sys"
N_ACCEPTANCE = "acceptance"
N_RNA        = "vaccRna_%d"
N_STD        = "vaccStd_%d"
##AUX VAR
#SSQ KINDS
RNA = "RNA"
STD = "STD"

INFTY = float("inf")

UTF8_MATH_IN_PRINT = True   #TODO shell compatibility??
MATH_IN_ORD        = 8712
DEBUG = False
if "DEBUG" in env and "T" in env["DEBUG"].upper():  DEBUG = True
AUDIT = True
if "AUDIT" in env and "F" in env["AUDIT"].upper():  AUDIT = False

ENV_CONFIG_HELP="\n\nexport:\tSTOP STOP_TRANSIENT_ANY STOP_STEADY_SEARCH STOP_MODE SAMPLING_MODE\n\tSAMPLE_STATS RUNTIMESAMPLING VACC_LINE_MODE NREPL DFLT_SEED DEBUG AUDIT"
